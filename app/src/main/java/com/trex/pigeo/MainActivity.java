package com.trex.pigeo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private Button submitData;
    private TextView emailId,userId,passwordView;
    private ProgressDialog progressDialog;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();
        submitData = findViewById(R.id.submitData);
        emailId = findViewById(R.id.email);
        userId = findViewById(R.id.userName);
        passwordView = findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        submitData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser(MainActivity.this);
            }
        });
    }
    private void registerUser(final Context context){
        String email = emailId.getText().toString().trim();
        String password = passwordView.getText().toString().trim();
        String userName = userId.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Email is Empty",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passwordView.getText().toString().trim())){
            Toast.makeText(this,"Password can't be empty",Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("Completing registration...");
        databaseReference.child("User : "+userName).setValue(email);
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(MainActivity.this,"Registration Successful!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    startActivity(new Intent(context,Main2Activity.class));
                }
                else{
                    Toast.makeText(MainActivity.this,"Connection error or Already registered",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }
}
